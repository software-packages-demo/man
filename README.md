# man

Manual page. https://en.m.wikipedia.org/wiki/Man_page

# At least 2 implementation
* mandb
* mandoc

# Tools to use with man
* bat

# A few projects using man
* pacman-packages-demo / mandoc
* apt-packages-demo / mandoc
* dnf-packages-demo / mandoc
* apk-packages-demo / man
